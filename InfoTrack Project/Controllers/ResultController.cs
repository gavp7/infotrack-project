﻿//ResultController.cs
//Written by Gavin Palmer
//Last Edited 13/03/17
//This Controller scrapes the data from Google based on user inputs and returns the urls which appear and their position within Google
using InfoTrack_Project.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace InfoTrack_Project.Controllers
{
    [Route("api/result/{ad}/{input}")]
    public class ResultController : ApiController
    {
        public List<Result> getResults(string ad, string input)
        {
            Debug.WriteLine(ad + " " + input);
            string html = string.Empty;
            string search = input; //search field
            string website = ad;
            List<Result> results = new List<Result>(); //list of results 
            Debug.WriteLine("Retrieving Search Results...");

            search = search.Replace(" ", "+"); //replace the spaces with + for the url
            string url = "https://www.google.co.uk/search?num=100&q=" + search; //url as taken from the project spec

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd(); //store html returned from url in string 
            }
            Regex regex = new Regex(@"");
            string returned = "(?s)<h3[^>]*?class=\"r\"[^>]*?>(.*?)</h3>";
            int count = 0; //counter to keep track of position of result on google

            foreach (Match match in Regex.Matches(html, returned)) //every time the div in the regex is found 
            {
                count++; //increment counter

                string totext = match.ToString(); //convert Match to string so it can be parsed further
                string site = website; //go back and get better regex although this seems to work for now 
                
                //only do this to the divs which contain the specified website 
                foreach (Match m in Regex.Matches(totext, site))
                {
                    //regex looking for urls between these characters as observed in the html
                    string addressstart = Regex.Escape("<a href=\"/url?q=");
                    string addressend = Regex.Escape(@"&amp");
                    string regAdd = addressstart + "(.*?)" + addressend; 

                    Regex adReg = new Regex(regAdd);
                    Match adMatch = adReg.Match(totext);
                    //create a Result instance and store the position of the search and the address
                    if(adMatch.Success){
                        string address = adMatch.Groups[1].Value;
                        Result r = new Result(count, address);
                        Debug.WriteLine(r.url + " " + r.position);
                        results.Add(r); //add result to the list
                        Debug.WriteLine(results.Count());
                    }
                }
            }
            return results;
        }
        }
    }

