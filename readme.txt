Hi!
Thanks for taking the time to look at my project.
I have included the project spec in this folder for quick reference.

The back end of the project which scrapes google based on inputs was created using C# .NET in Visual Studio 2013. 
I built the front end using Angularjs in Sublime Text. 

To get the project working, open the visual studio project and run it. This should just run in the background without any problem.
You can then open the web page statically by navigating to the file in the directory in your browser (InfoTrack Project/front end - Angular/public/index.html). 

As far as I can tell the system should run fine without needing any additional libraries.  I have included a very short video demo of the project running to ensure you can see the system running.

Thanks again for looking at my project. I had a lot of fun building it and any feedback or suggestions for improvement would be greatly appreciated! 