﻿//Result.cs
//Written by Gavin Palmer
//Result Class to store details on each result returned
//Last Edited 12/3/17

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InfoTrack_Project.Models
{
    public class Result
    {
        //variables
        public int position {get; set; }
        public string url { get; set; }

        public Result(int p, string u) //constructor
        {
            position = p;
            url = u;
        }
       
    }




}