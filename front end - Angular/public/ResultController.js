//controller.js
angular.module('myApp', [])
.controller('resultController', ['$scope', 'ResultFactory', function($scope, ResultFactory) {
	$scope.getResults = function(address, search){ 
		$scope.loading = true;
		//empty the results to display loading 
		$scope.results = null;
		$scope.count = 0;
		$scope.time = "times";
		//call the factory to get data
		ResultFactory.GetResults(address, search).then(function(response) {
			$scope.results = response.data;
			$scope.count = $scope.results.length;

			if($scope.count == 1){ //keep message at bottom gramatically correct
				$scope.time = "time";
			}
		});
	}
	//preset the search fields to save time typing this in every day, however they can be changed by the user 
	$scope.address = 'www.infotrack.co.uk';
	$scope.search = 'Land Registry Search';
	$scope.loading = false;
}]);

angular.module('myApp')
.factory('ResultFactory', function($http) {
	var url = 'http://localhost:17455/api/result/'; //url
	return {
		GetResults : function (address, search) {
			return $http.get(url + address + '/' + search) //get the data with the parameters
		}
	}
});
